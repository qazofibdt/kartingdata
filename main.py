import argparse
from cachedapi import CachedApi, get_data_path
import csv
import signal
import sys
import os

shutdown_signal = False

def ctrlc_handler(signal, frame):
  print('\nCtrl-C detected. Shutting down safely')
  global shutdown_signal
  shutdown_signal = True

signal.signal(signal.SIGINT, ctrlc_handler)

parser = argparse.ArgumentParser(description=
    'Downloads and caches karting data from the given location')

parser.add_argument('location',
    help='a location code, as used by ClubSpeed')

args = parser.parse_args()
location = args.location

api = CachedApi(location)

racer_fieldnames = [
  'Racer ID',
  'Racer Name',
  'Racer Points',
]

heat_fieldnames = [
  'Heat ID',
  'Heat Name',
  'Win By',
  'Date-Time',
]

racer_heat_fieldnames = [
  'Heat ID',
  'Racer ID',
  'Kart Number',
  'Best Lap Time',
  'Final Position',
  'Heat\'s Effect on Points',
  'Points Before Heat'
]

lap_fieldnames = [
  'Heat ID',
  'Racer ID',
  'Lap Number',
  'Lap Time',
  'Lap Position'
]

def get_heat_from_racer_info(racer, heat_id):
  for heat_details in racer['heats']:
    if heat_details['heat']['id'] == heat_id:
      return heat_details
  return None

def get_racer_laps_from_heat(heat, racer_id):
  for racers_laps in heat['laps']:
    if racers_laps['racerId'] == racer_id and 'racerLaps' in racers_laps:
      return racers_laps['racerLaps']
  return None

def process_heat(heat_id, heats_file, racers_file, racer_heats_file, laps_file, processed_racers):
  #print('Querying heat: ' + str(heat_id))
  heat = api.get_heat_info(heat_id)

  if heat is None:
    return False

  # trim the root from the JSON object
  heat = heat['heat']

  for participant in heat['participants']:
    racer_id = participant['id']

    #print('Querying racer: ' + str(racer_id))
    racer = api.get_racer_info(racer_id)

    # The occasional heat lists participants that don't exist. In these cases, ignore the entire
    # heat
    if racer is None:
      return False

    # trim the root from the object
    racer = racer['racer']

    heat_details_for_racer = get_heat_from_racer_info(racer, heat_id)

    # If the racer info doesn't mention the heat we got their ID from, the racer info must be out
    # of date, so delete the cache to force a re-download
    if heat_details_for_racer is None:
      api.delete_racer_cache(racer_id)
      racer = api.get_racer_info(racer_id)['racer']

      heat_details_for_racer = get_heat_from_racer_info(racer, heat_id)

    for h in racer['heats']:
      if h['heat']['id'] == heat_id:
        racer_heat = h
        break;
    else:
      print('\n\nHeat ' + str(heat_id) + ' listed racer ' + str(racer_id) + ' as a participant, ' +
            'but there is no record of that heat in the racer\'s info. This probably means the ' +
            'data is recent and still being updated. We cannot continue with incomplete data, so ' +
            'we will terminate immediately\n')
      global shutdown_signal
      shutdown_signal = True
      return False

    # If this is the first time we've seen this racer, write out their basic info
    if racer_id not in processed_racers:
      racers_file.writerow({
        'Racer ID': racer_id,
        'Racer Name': racer['racerName'],
        'Racer Points': racer['points']
      })

      processed_racers.add(racer_id)

    # Write out details about this racer's performance in this heat
    racer_heats_file.writerow({
      'Heat ID':                  heat_id,
      'Racer ID':                 racer_id,
      'Kart Number':              racer_heat['kartNumber'],
      'Best Lap Time':            racer_heat['bestLapTime'],
      'Final Position':           racer_heat['finalPosition'],
      'Heat\'s Effect on Points': racer_heat['pointsImpact'],
      'Points Before Heat':       racer_heat['pointsAtStart']
    })

    laps = get_racer_laps_from_heat(heat, racer_id)

    # Write out lap information for this racer in this heat
    # TODO remove if statement when laps is a required property
    if laps is not None:
      for lap in laps:
        laps_file.writerow({
          'Heat ID':      heat_id,
          'Racer ID':     racer_id,
          'Lap Number':   lap['lapNumber'],
          'Lap Time':     lap['seconds'],
          'Lap Position': lap['position']
        })

  # Write out heat info
  heats_file.writerow({
    'Heat ID':   heat_id,
    'Heat Name': heat['name'],
    'Win By':    heat['winBy'],
    'Date-Time': heat['localDateTime']
  })

  return True

# Persist the passed heat ID, indicating that it and every prior heat have been
# processed
def save_last_heat(heat_id):
  with open(last_heat_path, 'w+') as last_heat_file:
    last_heat_file.write(str(heat_id))

def append_csv(from_filepath, to_file, predicate=None):
  if os.path.isfile(from_filepath):
    with open(from_filepath) as from_file:
      from_csv_reader = csv.DictReader(from_file)

      assert from_csv_reader.fieldnames == to_file.fieldnames, \
          'Logic error: trying to append CSV with field names {} to CSV with field names {}'.format(
                 from_csv_reader.fieldnames, to_file.fieldnames)

      for row in from_csv_reader:
        if predicate is None or predicate(row):
          to_file.writerow(row)

data_path = get_data_path(location)

if not os.path.exists(data_path):
  os.makedirs(data_path)

last_heat_path = data_path + 'lastheat'

heats_csv_tmp_path = data_path + 'heats-tmp.csv'
racers_csv_tmp_path = data_path + 'racers-tmp.csv'
racer_heats_csv_tmp_path = data_path + 'racer-heats-tmp.csv'
laps_csv_tmp_path = data_path + 'laps-tmp.csv'

heats_csv_path = data_path + 'heats.csv'
racers_csv_path = data_path + 'racers.csv'
racer_heats_csv_path = data_path + 'racer-heats.csv'
laps_csv_path = data_path + 'laps.csv'

heats_csv_backup_path = data_path + 'heats.csv.bak'
racers_csv_backup_path = data_path + 'racers.csv.bak'
racer_heats_csv_backup_path = data_path + 'racer-heats.csv.bak'
laps_csv_backup_path = data_path + 'laps.csv.bak'

# Figure out which heat ID we should start with. This is either one greater
# than the last good heat we saw last time we ran this program, or zero
try:
  with open(last_heat_path) as last_heat_file:
    start_heat = int(last_heat_file.readline()) + 1
except IOError as e:
  start_heat = 0

next_heat_id = start_heat
fail_count = 0

processed_racers = set()

with open(heats_csv_tmp_path, 'w+') as heats_file, \
     open(racers_csv_tmp_path, 'w+') as racers_file, \
     open(racer_heats_csv_tmp_path, 'w+') as racer_heats_file, \
     open(laps_csv_tmp_path, 'w+') as laps_file:

  heats_csv_writer = csv.DictWriter(heats_file, fieldnames=heat_fieldnames)
  racers_csv_writer = csv.DictWriter(racers_file, fieldnames=racer_fieldnames)
  racer_heats_csv_writer = csv.DictWriter(racer_heats_file, fieldnames=racer_heat_fieldnames)
  laps_csv_writer = csv.DictWriter(laps_file, fieldnames=lap_fieldnames)

  heats_csv_writer.writeheader()
  racers_csv_writer.writeheader()
  racer_heats_csv_writer.writeheader()
  laps_csv_writer.writeheader()

  # Start by writing new data to the tmp files
  while fail_count < 300 and not shutdown_signal:
    try:
      success = process_heat(next_heat_id, heats_csv_writer, racers_csv_writer,
                             racer_heats_csv_writer, laps_csv_writer, processed_racers)
    except e:
      # If anything goes wrong, print the error and stop collecting new data
      print('Something went wrong at heat {}. Stopping data collection'.format(next_heat_id))
      print(e)
      break

    if not success:
      fail_count += 1
    else:
      fail_count = 0
      save_last_heat(next_heat_id)

    next_heat_id += 1

  print('\nCombining new and old data...')

  # Go through the old files and append the old data to the tmp files. We're appending the old data
  # to the new file, rather than the new data to the old file, to maintain the integrity of the
  # output files. If we crash somewhere in here, the tmp file will be corrupt, not the main file
  append_csv(heats_csv_path, heats_csv_writer)
  # Updated racer info overrides old rader info, so only append old records with racer IDs that we
  # did not process on this run
  append_csv(racers_csv_path, racers_csv_writer,
             lambda row: int(row['Racer ID']) not in processed_racers)
  append_csv(racer_heats_csv_path, racer_heats_csv_writer)
  append_csv(laps_csv_path, laps_csv_writer)

  # The racers file needs to be handled differently. Updated racer info overrides old racer info, so
  # we should ignore old records for racer IDs we processed during this run

# Remove old backups, if they exist
if os.path.isfile(heats_csv_backup_path):
  os.remove(heats_csv_backup_path)
if os.path.isfile(racers_csv_backup_path):
  os.remove(racers_csv_backup_path)
if os.path.isfile(racer_heats_csv_backup_path):
  os.remove(racer_heats_csv_backup_path)
if os.path.isfile(laps_csv_backup_path):
  os.remove(laps_csv_backup_path)

# Back up the old files
if os.path.isfile(heats_csv_path):
  os.rename(heats_csv_path, heats_csv_backup_path)
if os.path.isfile(racers_csv_path):
  os.rename(racers_csv_path, racers_csv_backup_path)
if os.path.isfile(racer_heats_csv_path):
  os.rename(racer_heats_csv_path, racer_heats_csv_backup_path)
if os.path.isfile(laps_csv_path):
  os.rename(laps_csv_path, laps_csv_backup_path)

# Swap the new files for the old ones
os.rename(heats_csv_tmp_path, heats_csv_path)
os.rename(racers_csv_tmp_path, racers_csv_path)
os.rename(racer_heats_csv_tmp_path, racer_heats_csv_path)
os.rename(laps_csv_tmp_path, laps_csv_path)
 
last_good_heat_id = next_heat_id - fail_count - 1

# Delete caches for heat IDs that may be valid but not yet populated
for bad_heat_id in range(last_good_heat_id + 1, next_heat_id):
  api.delete_heat_cache(bad_heat_id)

num_caches_deleted = next_heat_id - (last_good_heat_id + 1)

# Print out some information
print('Last good heat: ' + str(last_good_heat_id))

if num_caches_deleted > 0:
  print('Deleted heat caches from {} to {} inclusive'.format(last_good_heat_id + 1,
                                                             next_heat_id - 1))

print('Have a nice day!')
