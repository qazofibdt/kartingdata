import json
import jsonschema
from kartlapsapi import KartlapsApi
import sys
import os
from enum import Enum

class InfoKind(Enum):
  Racer = {
    'name': 'racer',
    'symbols': {
      'download':       ';',
      'cache':          '.',
      'download-none':  'Y',
      'cache-none':     'y'
    }
  }
  Heat = {
    'name': 'heat',
    'symbols': {
      'download':       '!',
      'cache':          '|',
      'download-none':  'H',
      'cache-none':     'h'
    }
  }

def get_cache_path(location, kind):
  return get_data_path(location) + 'cache/' + kind.value['name'] + '-data/'

def create_cache_dirs(location):
  cache_paths = [ get_cache_path(location, kind) for kind in InfoKind ]

  for cache_path in cache_paths:
    if not os.path.exists(cache_path):
      os.makedirs(cache_path)

def get_data_path(location):
  return 'data/' + location + '/'

def create_dirs(location):
  cache_paths = [get_cache_path(location, kind) for kind in InfoKind]

  # The cache path includes the data path, so this call will create both if
  # they don't exist
  for path in cache_paths:
    if not os.path.exists(path):
      os.makedirs(path)

def print_symbol(symbols, name):
  print(symbols[name], end='', flush=True)

class RedownloadInfo(Exception):
  pass

class CachedApi:
  def __init__(self, location):
    self.location = location
    self.api = KartlapsApi(location)

    create_dirs(location)

  def get_info(self, kind, item_id):
    kind_name = kind.value['name']
    symbols = kind.value['symbols']

    cache_file_path = get_cache_path(self.location, kind)+str(item_id)
    schema_file_path = 'response-schemas/' + kind_name + '.schema.json'

    try:
      # Try opening the file in the data folder whose name is the racer's
      # ID. If the file doesn't exist, we can't open it, or its contents
      # cannot be parsed as JSON, then we need to redownload the racer info
      try:
        with open(cache_file_path) as cache_file:
          info = json.load(cache_file)
      except IOError as e:
        raise RedownloadInfo
      except ValueError as e:
        print('\nWARNING: Corrupt ' + kind_name + ' info file, redownloading')
        raise RedownloadInfo

      # If the info retrieved from the cache is an empty object, this
      # indicates that the requested item is known to be missing. Translate
      # the empty JSON object to None. If the info is not None, we need to
      # validate it
      if not info:
        info = None
      else:
        # Load the schema for this kind of info. If we can't, fail
        try:
          with open(schema_file_path) as schema_file:
            schema = json.load(schema_file)
        except IOError as e:
          print('\n\nFATAL: Unable to open schema: ' + schema_file_path)
          sys.exit()
        except ValueError as e:
          print('\n\nFATAL: Malformed schema\n\n' + str(e))
          sys.exit()

        # Validate the file contents against the racer info schema. If
        # validation fails, something is wrong with the file and we need to
        # redownload the racer info
        try:
          jsonschema.validate(info, schema)
        except jsonschema.exceptions.ValidationError as e:
          print('\nWARNING: Invalid cached ' + kind_name + ' info, redownloading')
          #print(str(e))
          raise RedownloadInfo

      # Great! We've opened, parsed, and validated cached racer info and
      # it's up-to-date, we don't need to download anything. We'll handle
      # returning the info in the 'else' clause, because that seems most
      # readable

    except RedownloadInfo as e:
      info = getattr(self.api, 'get_' + kind_name)(item_id)

      # The data we want to write out is the serialized JSON, or an empty
      # JSON object if there is no data for this item
      if info is None:
        cache_str = '{}'
      else:
        cache_str = json.dumps(info)

      with open(cache_file_path, 'w+') as cache_file:
        cache_file.write(cache_str)

      if info is None:
        print_symbol(symbols, 'download-none')
      else:
        print_symbol(symbols, 'download')

      return info
    else:
      if info is None:
        print_symbol(symbols, 'cache-none')
      else:
        print_symbol(symbols, 'cache')

      return info

  def get_heat_info(self, heat_id):
    return self.get_info(InfoKind.Heat, heat_id)

  def get_racer_info(self, racer_id):
    return self.get_info(InfoKind.Racer, racer_id)

  def delete_cache(self, kind, item_id):
    cache_path = get_cache_path(self.location, kind) + str(item_id)

    if os.path.isfile(cache_path):
      os.remove(cache_path)

  def delete_heat_cache(self, heat_id):
    self.delete_cache(InfoKind.Heat, heat_id)

  def delete_racer_cache(self, racer_id):
    self.delete_cache(InfoKind.Racer, racer_id)

