from enum import Enum
import json
import jsonschema
import os
import requests
from requests.exceptions import ConnectionError
import sys

API_URL = 'http://kartlaps.info/v2/'

class UnexpectedResponseError(Exception):
  def __init__(self, msg, response):
    self.response = response
    self.msg = msg

  def __str__(self):
    return self.msg + '\n\n' + str(self.response)

class UnsupportedCommandError(Exception):
  def __init__(self, command):
    self.command = command

  def __str__(self):
    return 'Unsupported command: ' + command

class KartlapsApi:
  def __init__(self, location):
    self.location = location
    self.session = requests.Session()

  def _single_request(self, command, param=None):
    """
    Send a single request to kartlaps.info. For example, this is a request:
    http://kartlaps.info/mb2sylmar/racer-search/estes
    where racer-search and estes are the command and the param
    respectively. mb2sylmar is the location name, as passed to the
    initializer of this class.
    
    Returns JSON that conforms to the corresponding
    request-schema/<command>.schema.json schema file.

    Note that the schemas only validate the properties we support, and they
    generally allow arbitrary additional properties to be present.

    Raises an UnsupprtedRequestError if the passed command does not have a
    corresponding .schema.json file

    Raises an UnexpectedResponseError if the response status is not 200,
    the response text cannot be parsed as JSON, or the parsed JSON fails
    validation against the schema
    """

    schema_dir_abs_path = os.path.join(os.getcwd(), 'response-schemas')
    schema_filename = os.path.join(schema_dir_abs_path, command + '.schema.json')

    # Open the schema file for this request. If we can't open the file,
    # we'll assume that the schema is missing and therefore that the
    # request is unsupported.
    try:
      with open(schema_filename) as schema_file:
        schema = json.load(schema_file)
    except IOError as e:
      raise UnsupportedCommandError(command)
    except ValueError as e:
      print('FATAL: Malformed schema\n\n' + str(e))
      sys.exit()

    # Construct the URL
    url_components = [self.location, command]
    if param is not None:
      url_components.append(param)

    request_str = API_URL + '/'.join(url_components)

    res = None
    try:
      # Send the request
      res = self.session.get(request_str)
    except ConnectionError:
      print('\nWarning: Connection error. Retrying...')
      res = self.session.get(request_str)
      print('Retry successful. Continuing')

    # 400 code is used to indicate a non-existent record
    if res.status_code != 200 and res.status_code != 400:
      raise UnexpectedResponseError('Unexpected status code: ' +
          str(res.status_code), res)

    # Parse the request as JSON
    try:
      res_json = res.json()
    except ValueError as e:
      raise UnexpectedResponseError(
        request_str + '. Error occurred while trying to parse response as JSON: ' + str(e),
        res)

    # Validate the response against the schema
    try:
      jsonschema.validate(res_json, schema)
    except jsonschema.exceptions.ValidationError as e:
      raise UnexpectedResponseError('Response failed validation', res)
    except jsonschema.exceptions.SchemaError as e:
      print('FATAL: Malformed schema\n\n' + str(e))

    return res_json

  def get_racer(self, racer_id):
    """
    Get information on the racer with the given ID.

    Returns None if no racer exists with the given ID, otherwise returns an
    object that conforms to racer.schema.json
    """

    res = self._single_request('racer', str(racer_id))

    return res if 'racer' in res else None

  def get_heat(self, heat_id):
    """
    Get information on the heat (race) with the given ID

    Returns None if no heat exists with the given ID, otherwise returns an
    object that conforms to heat.schema.json
    """

    res = self._single_request('heat', str(heat_id))

    # Some heats that have not yet happened get returned without errors but
    # without any participants. A heat like this often has real data put
    # into it once the heat actually occurs, so we want to indicate to our
    # clients that this heat doesn't exist, so that they know to check it
    # again later
    if 'heat' in res and 'participants' in res['heat']:
      return res
    else:
      return None

