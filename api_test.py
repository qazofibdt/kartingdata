import kartlapsapi
import unittest

api = kartlapsapi.KartlapsApi('tirwashington')

qazId = 1046927
qazName = 'qazofibdt'
qazHeatId = 11681

class TestAPI(unittest.TestCase):

  def test_get_racer(self):
    racer = api.get_racer(qazId)
    self.assertEqual(racer['racerName'], qazName)
    self.assertTrue(len(racer['heats']) > 100)

    bad_res = api.get_racer(0)
    self.assertIsNone(bad_res)

  def test_get_heat(self):
    heat = api.get_heat(qazHeatId)
    self.assertEqual(heat['name'], 'Indoor Drop-In')
    self.assertTrue(
      any(p['racerName'] == qazName for p in heat['participants']))
    self.assertTrue(heat['laps'])

    self.assertTrue(
      any(r['racerId'] == qazId for r in heat['laps']))

    bad_res = api.get_heat(0)
    self.assertIsNone(bad_res)

unittest.main()

