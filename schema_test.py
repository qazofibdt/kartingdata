import json
import jsonschema
import unittest

class TestSchemas(unittest.TestCase):

  def load_json(self, filepath):
    try:
      with open(filepath) as f:
        contents = f.read()
    except IOError as e:
      self.fail("File not found, or unable to open: " + filepath)

    try:
      res = json.loads(contents)
    except ValueError as e:
      self.fail("Unable to parse file contents as json: " + filepath)

    return res

  def do_test_command(self, schema_name, test_name, expect_success):
    success = True

    schema_filepath = "response-schemas/" + schema_name + ".schema.json"
    test_filepath = "test-files/" + test_name + ".test.json"
    
    schema_json = self.load_json(schema_filepath)
    test_json = self.load_json(test_filepath)

    try:
      jsonschema.validate(test_json, schema_json)
    except jsonschema.exceptions.ValidationError as e:
      success = False
    except jsonSchema.exceptions.SchemaError as e:
      self.fail("Malformed schema: " + schema_filepath)

    self.assertEqual(success, expect_success)

  def test_racer_search(self):
    self.do_test_command("racer-search", "racer-search", True)

  def test_leaderboard(self):
    self.do_test_command("leaderboard", "leaderboard", True)

  def test_racer(self):
    self.do_test_command("racer", "racer-exists", True)
    self.do_test_command("racer", "racer-none", True)
    self.do_test_command("racer", "racer-none-bad", False)
    self.do_test_command("racer", "racer-missing-points", False)
    self.do_test_command("racer", "racer-heat-missing-name", False)

  def test_heat(self):
    self.do_test_command("heat", "heat-exists", True)
    self.do_test_command("heat", "heat-none", True)
    self.do_test_command("heat", "heat-none-bad", False)
    self.do_test_command("heat", "heat-participant-missing-name", False)

unittest.main()
